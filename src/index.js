import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'

function Square(props) {
  const classes = props.isWinningSquare ? "square active" : "square"
  return (
      <button className={classes} onClick={props.onClick}>
        {props.value}
      </button>
  )
}

class Board extends React.Component {
  renderSquare(i, isWinningSquare) {
    return (
        <Square
            key={i}
            value={this.props.squares[i]}
            onClick={() => this.props.onClick(i)}
            isWinningSquare={isWinningSquare}
        />
    )
  }

  createGrid(winningSquares) {
    let content = []
    const cells = 9

    for (let i = 0; i < cells; i++) {
      content.push(this.renderSquare(i, winningSquares.includes(i)))
    }

    return content
  }

  render() {
    let winningSquares = this.props.winningSquares ?? []
    const grid = this.createGrid(winningSquares)

    return (
        <div>
          <div className="board-row">
            {grid.slice(0,3)}
          </div>
          <div className="board-row">
            {grid.slice(3,6)}
          </div>
          <div className="board-row">
            {grid.slice(6,9)}
          </div>
        </div>
    )
  }
}

class Game extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      history: [{
        squares: Array(9).fill(null)
      }],
      lastPosition: [{
        x: null,
        y: null
      }],
      stepNumber: 0,
      xIsNext: true
    }
  }

  handleClick(i) {
    const x = i % 3
    const y = Math.floor(i / 3)

    const history = this.state.history.slice(0, this.state.stepNumber + 1)
    const current = history[history.length - 1]
    const squares = current.squares.slice()

    // if there is a winner or square is occupied, return
    if (calculateWinner(squares) || squares[i]) {
      return
    }

    squares[i] = this.state.xIsNext ? 'X' : 'O'   // fill square based on whose turn it is

    this.setState({
      // concat is better than push because it does not mutate the original array
      history: history.concat([{
        squares: squares
      }]),
      lastPosition: this.state.lastPosition.concat([{
        x: x,
        y: y
      }]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext
    })
  }

  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0
    })
  }

  render() {
    const history = this.state.history
    const current = history[this.state.stepNumber]
    const winner = calculateWinner(current.squares)

    const moves = history.map((step, move) => {
      const desc = move ?
          `Go to move #${move} (${this.state.lastPosition[move].x}, ${this.state.lastPosition[move].y})`
          :
          'Go to game start'
      return (
          <li key={move}>
            <button onClick={() => this.jumpTo(move)}>{desc}</button>
          </li>
      )
    })

    let status = (winner) ? `Winner: ${winner.winner}`
        : `Next player: ${this.state.xIsNext ? 'X' : 'O'}`

    return (
        <div className="game">
          <div className="game-board">
            <Board
                squares={current.squares}
                onClick={i => this.handleClick(i)}
                winningSquares={winner ? winner.cells : null}
            />
          </div>
          <div className="game-info">
            <div>{status}</div>
            <ol>{moves}</ol>
          </div>
        </div>
    )
  }
}

// helper function for calculating winner
function calculateWinner(squares) {
  // squares from the state are passed to the function
  const lines = [
    // these are the lines which, if they all are the same, then the occupier has won
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    // loop through each line
    const [a, b, c] = lines[i]; // get values from each line
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      // check if the value in Squares array for the three integers matches
      return ({
        cells: [a, b, c],
        winner: squares[a]
      });
    }
  }
  // if there is no winner, return null so the game keeps playing
  return null;
}

// ========================================

const root = ReactDOM.createRoot(document.getElementById("root"))
root.render(<Game/>)
