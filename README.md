# React Tutorial

Link: https://reactjs.org/tutorial/tutorial.html

## What is React?

Used to build UIs from small, isolated pieces of code, called `components`.

### `React.Component`

```javascript
class ShoppingList extends React.Component {
  render() {
    return (
      <div className="shopping-list">
        <h1>Shopping List for {this.props.name}</h1>
        <ul>
          <li>Instagram</li>
          <li>WhatsApp</li>
          <li>Oculus</li>
        </ul>
      </div>
    );
  }
}

// Example usage: <ShoppingList name="Mark" />
```

- Components are used to tell React what we want to see on the screen
- When the data changes, React efficiently updates and re-renders the components
- `ShoppingList` is a React component class/type
- Components take in parameters, called `props` (properties), and returns views to display using the `render` method
- `render` returns a description of what we want to see on screen, which is a **React element**

### `JSX`

- `JSX` syntax (above, looks like HTML) makes these structures easier to write
  - At runtime, `<div/>` is transformed to `React.createElement('div')`

Above example is equivalent to:
```javascript
return React.createElement('div', {className: 'shopping-list'},
  React.createElement('h1', /* ... h1 children ... */),
  React.createElement('ul', /* ... ul children ... */)
);
```

- JSX comes with the full power of JS, so any valid JS expressions can be put inside `{braces}`
- Each React element is a JavaScript object, that can be stored in variables and passed around in the program
- The above example only renders built-in DOM components, but entire React components can also be rendered by using `<ShoppingList />`, for example
- This demonstrates how small individual components can be created independently, and subsequently rendered into complex UIs

### State

- Components can 'remember' things using `state`
- React components can have a state by setting `this.state` in their constructors
- `this.state` should be private to a React component that it is defined in

```javascript
class Square extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: null,
    }
  }
  
  // class implementation... 
}
```

- The above snippet creates a state for the object called `value`, which can be updated
- `this.setState({value: 'X'})` can be used to update the state of an object
- When `setState` is called in a component, React automatically updates child components inside it

### State in the game

- In tic-tac-toe, in order to decide a winner, the state of each square needs to be available in a single location
- It makes sense to keep the game's state in the Board component
- The Board component can tell each Square what to display by passing a prop
- To collect data from multiple children or have two components communicate with each other, the shared state needs to be declared in the parent component
- Parent component passes state back down to the children using props, keeping child components in sync with each other and the parent component
- `Lifting state` into a parent component is common
- After making the changes to Square and Board, Square components are now **controlled components**

## Immutability

- In the code example, `slice()` is used when modifying the array
- Two approaches when changing data:
  - `mutate` the data by directly changing the data's values
  - replace the data with new copy which has the desired changes

### With mutation

```javascript
var player = {score: 1, name: 'Jeff'}
player.score = 2
// now player is {score: 2, name: 'Jeff'}
```

### Without mutation

```javascript
var player = {score: 1, name: 'Jeff'}
var newPlayer = {...player, score: 2}

// player is unchanged, and newPlayer is {score: 2, name: 'Jeff'}
```

The end result is the same, but there are several benefits of not mutating directly:

- Avoiding direct data mutation lets us keep previous versions of data intact, which can be used as a history
- Detecting changes in immutable objects is easy - if the object that is being referenced is different than the previous one, then the object has changed
- Helps us build _pure components_ in React - can easily determine if changes are made, which tells React to re-render the component

## Function Components

- Simpler way to write components that only contain a `render` method and don't have their own state
- Write a function that takes `props` as a parameter, and returns what should be rendered
- We only pass the `onClick` function reference to `onClick`, rather than actually calling the method (like in normal components)

```javascript
function Square(props) {
  return (
    <button className="square" onClick={props.onClick}>
      {props.value}
    </button>
  )
}
```

## Adding 'time travel'

This section covers the ability to recall the state of the board at each move.
It builds off the idea of immutability, where we decided to create a new array after each move,
rather than mutating the same array each time. If mutations had been used, this task would be very
challenging - immutability makes it much easier.

After each move, the state of the board will be saved in another array called `history`.
In order to achieve this, the state (of the Board in this case) must be lifted again to the Game component.

- The Board component loses the `handleClick` method and its constructor (state), and all it is responsible for now is rendering the board
- The state of the history, whose turn it is, and the methods for handling clicks are now all within the Game component

### Using `key` props

- When rendering a list, React needs to know what has happened to the list
- We use a `key` property for each list item so each element can be differentiated from other elements
- When lists are re-rendered, React takes each list items key and searches the previous list's items for a matching key
- If list has a key that didn't exist before, React creates a component
- If current list missing a key that existed in previous list, React destroys that component
- If two keys match, corresponding component is moved, etc.
- It allows React to keep track of how a list should be rendered when dealing with re-rendering

#### How `key` works

- When an element is created, React extracts the `key` property and stores the key directly on the returned element
- Even though `key` looks like a prop, `key` cannot be referenced using `this.props.key` - a component can not inquire about its key
- **Strongly recommended that you assign proper keys whenever building dynamic lists**
- With no key specified, React will use the array index, which can be problematic when reordering items
- Keys do not need to be globally unique, only need to be unique between components and their siblings
